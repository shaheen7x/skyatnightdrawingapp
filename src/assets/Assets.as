package assets
{
	import flash.display.Bitmap;
	import flash.filesystem.File;
	
	import core.MainStarling;
	
	import starling.assets.AssetManager;

	public class Assets
	{
		[Embed(source="graphics/watermark.png")]
		private static const Watermark:Class;
		
		public static var assetsManager:starling.assets.AssetManager = new AssetManager;
		private static var mainApp:MainStarling;
		
		public static function init(_mainApp:MainStarling):void
		{
			mainApp = _mainApp;
			var appDir:File = File.applicationDirectory;
			trace(appDir.url);
			trace(appDir.nativePath);
			assetsManager.enqueue(appDir.resolvePath("assets"));	
			assetsManager.loadQueue(QueueLoading,null,QueueProgress);
		}
		private static function QueueProgress(ratio:Number):void
		{
//			trace("Loading assets, progress:", ratio);	
		}
		private static function QueueLoading():void
		{
				mainApp.StartApp();
		}
		public static function getWatermark():Bitmap
		{
			var bitmap:Bitmap = new Watermark;
			bitmap.smoothing = true;
			return bitmap;
		}
	}
}