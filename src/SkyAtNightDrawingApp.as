package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import core.MainStarling;
	
	import data.Settings;
	
	import objects.DrawingArea;
	
	import starling.core.Starling;
	
	[SWF(width="1000", height="750", frameRate="60", backgroundColor="#000000")]
	
	public class SkyAtNightDrawingApp extends Sprite
	{
		private var _starling:Starling;
		private var drawingArea:DrawingArea;
		public function SkyAtNightDrawingApp()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.nativeWindow.x = (stage.fullScreenWidth - Settings.APPLICATION_START_WIDTH)/2;
			stage.nativeWindow.y = (stage.fullScreenHeight - Settings.APPLICATION_START_HEIGHT)/2;
//			stage.nativeWindow.
			stage.nativeWindow.maximize();
//			stage.nativeWindow.minimizable = false;
			
//			GlobalData.getAppData();
//			var viewPort:Rectangle = new Rectangle(0,0,stage.stageWidth,stage.stageHeight);
			
			
			Settings.starlingWidth = stage.stageWidth;
			Settings.starlingHeight = stage.stageHeight;
			Settings.settingsViewWidth = 250;
			Settings.settingsViewHeight = stage.stageHeight;
			Settings.infoViewWidth = stage.stageWidth - Settings.settingsViewWidth;
			Settings.infoViewHeight = 150;
			Settings.drawingAreaWidth = stage.stageWidth - Settings.settingsViewWidth;
			Settings.drawingAreaHeight = stage.stageHeight - Settings.infoViewHeight;

			
			
			initStarling();
			
			stage.addEventListener(Event.RESIZE, onWindowResize);
			
			
			
			
//			drawingArea = new DrawingArea;
//			this.addChild(drawingArea);
			
//			
//			var drawingArea:DrawingArea;
//			drawingArea = new DrawingArea;
//			this.addChild(drawingArea);
			
			
		}
		
		protected function onWindowResize(event:Event):void
		{
			stage.nativeWindow.maximize();
//			return
//			trace("NativeWindowDisplayState",stage.nativeWindow.displayState);
//			if(stage.nativeWindow.displayState == NativeWindowDisplayState.NORMAL)
//			{
//				_starling.viewPort.x = stage.stageWidth - Settings.starlingWidth;
			trace("stage.fullScreenWidth",stage.fullScreenWidth,"stage.fullScreenHeight",stage.fullScreenHeight);
			trace("stage.stageWidth",stage.stageWidth,"stage.stageHeight",stage.stageHeight);
			_starling.viewPort.width = stage.stageWidth;
			_starling.viewPort.height = stage.stageHeight;
			_starling.stage.stageWidth = stage.stageWidth;
			_starling.stage.stageHeight = stage.stageHeight;
			Settings.settingsViewHeight = stage.stageHeight;
			Settings.infoViewWidth = stage.stageWidth - Settings.settingsViewWidth;
			Settings.drawingAreaWidth = stage.stageWidth - Settings.settingsViewWidth;
			Settings.drawingAreaHeight = stage.stageHeight - Settings.infoViewHeight;
			//				drawingArea.resize();
//			_starling.render();
//			}
		}
		
		private function initStarling():void
		{
//			var viewPort:Rectangle = new Rectangle(stage.stageWidth - Settings.starlingWidth,0,Settings.starlingWidth,Settings.starlingHeight);
			var viewPort:Rectangle = new Rectangle(0,0,stage.stageWidth,Settings.starlingHeight);
			_starling = new Starling(MainStarling, stage, viewPort);
			_starling.stage.stageWidth = stage.stageWidth;
			_starling.stage.stageHeight = stage.stageHeight;
			_starling.start();
		}
	}
}