package objects
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import data.Settings;
	
	public class PanelDimentionHorizontalMark extends Sprite
	{
		private var lineHeight:Number = 24;
		private var lineArrowWidth:Number = 8;
		private var lineArrowHeadPadding:Number = 6;
		private var _size:Number;
		private var label:TextField;
		private var labelFormat:TextFormat;
		private var startLine:Shape;
		private var startArrow:Shape;
		private var sizeLine:Shape;
		private var endArrow:Shape;
		private var endLine:Shape;
		private var color:uint;
		public function PanelDimentionHorizontalMark()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			startLine = new Shape;
			this.addChild(startLine);
			
			startArrow= new Shape;
			this.addChild(startArrow);
			
			sizeLine = new Shape;
			this.addChild(sizeLine);
			
			endArrow= new Shape;
			this.addChild(endArrow);
			
			endLine = new Shape;
			this.addChild(endLine);
			
			labelFormat = new TextFormat("Calibri",16,0x257AC4);
			labelFormat.align = TextFormatAlign.CENTER;
			
			label = new TextField;
			
			this.addChild(label);
			
			
			label.background = true;
			label.backgroundColor = 0xffffff;
			
		}
		
		public function draw(size:Number):void
		{
			var scaleFactor:Number = Settings.drawingScaleFactor;
			_size = size;
			if(_size == 60 || _size == 120)
			{
				color = 0x257AC4;
			}else{
				color = 0xCE4C50;
			}
			labelFormat.color = color;
			label.defaultTextFormat = labelFormat;
			
			startLine.graphics.clear();
			startLine.graphics.lineStyle(1, color, 1);
			startLine.graphics.moveTo(0, 0);
			startLine.graphics.lineTo(0, lineHeight);
			
			startArrow.graphics.clear();
			endArrow.graphics.clear();
			if(_size  * scaleFactor > lineHeight)
			{
				startArrow.graphics.lineStyle(1, color, 1);
				startArrow.graphics.moveTo(lineArrowWidth, lineArrowHeadPadding);
				startArrow.graphics.lineTo(0, lineHeight /2);
				startArrow.graphics.lineTo(lineArrowWidth, lineHeight - lineArrowHeadPadding);
				
				endArrow.graphics.lineStyle(1, color, 1);
				endArrow.graphics.moveTo(_size  * scaleFactor - lineArrowWidth, lineArrowHeadPadding);
				endArrow.graphics.lineTo(_size  * scaleFactor, lineHeight /2);
				endArrow.graphics.lineTo(_size  * scaleFactor - lineArrowWidth, lineHeight - lineArrowHeadPadding);
			}
			
			sizeLine.graphics.clear();
			sizeLine.graphics.lineStyle(1, color, 1);
			sizeLine.graphics.moveTo(0, lineHeight /2);
			sizeLine.graphics.lineTo(_size  * scaleFactor, lineHeight /2);			
			
			endLine.graphics.clear();
			endLine.graphics.lineStyle(1, color, 1);
			endLine.graphics.moveTo(_size  * scaleFactor, 0);
			endLine.graphics.lineTo(_size  * scaleFactor, lineHeight);
			
			label.text = (_size * 10).toString();
			label.width = 40;
			label.height = lineHeight - 4;
			label.x = (_size  * scaleFactor - label.width) / 2;
			label.y = lineHeight - 4;
		}
	}
}