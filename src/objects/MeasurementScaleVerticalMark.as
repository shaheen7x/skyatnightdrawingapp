package objects
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import data.Settings;
	
	public class MeasurementScaleVerticalMark extends Sprite
	{
		private var lineHeight:Number = 24;
		private var lineArrowWidth:Number = 8;
		private var lineArrowHeadPadding:Number = 6;
		private var _size:Number;
		private var label:TextField;
		private var labelFormat:TextFormat;
		private var startLine:Shape;
		private var startArrow:Shape;
		private var sizeLine:Shape;
		private var endArrow:Shape;
		private var endLine:Shape;
		
		public function MeasurementScaleVerticalMark()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			startLine = new Shape;
			this.addChild(startLine);
			
			startArrow= new Shape;
			this.addChild(startArrow);
			
			sizeLine = new Shape;
			this.addChild(sizeLine);
			
			endArrow= new Shape;
			this.addChild(endArrow);
			
			endLine = new Shape;
			this.addChild(endLine);
			
			labelFormat = new TextFormat("Calibri",16);
			labelFormat.align = TextFormatAlign.CENTER;
			
			label = new TextField;
			label.defaultTextFormat = labelFormat;
			this.addChild(label);
			
			
			label.background = true;
			label.backgroundColor = 0xffffff;
			
		}
		
		public function draw(size:Number):void
		{
			var scaleFactor:Number = Settings.drawingScaleFactor;
			_size = size;
			startLine.graphics.clear();
			startLine.graphics.lineStyle(1, 0x111111, 1);
			startLine.graphics.moveTo(0, 0);
			startLine.graphics.lineTo(lineHeight, 0);
			
			startArrow.graphics.clear();
			startArrow.graphics.lineStyle(1, 0x111111, 1);
			startArrow.graphics.moveTo(lineArrowHeadPadding,lineArrowWidth);
			startArrow.graphics.lineTo(lineHeight /2 , 0);
			startArrow.graphics.lineTo(lineHeight - lineArrowHeadPadding, lineArrowWidth);
			
			sizeLine.graphics.clear();
			sizeLine.graphics.lineStyle(1, 0x111111, 1);
			sizeLine.graphics.moveTo(lineHeight /2, 0);
			sizeLine.graphics.lineTo(lineHeight /2,_size * scaleFactor);
			
			endArrow.graphics.clear();
			endArrow.graphics.lineStyle(1, 0x111111, 1);
			endArrow.graphics.moveTo(lineArrowHeadPadding, _size * scaleFactor - lineArrowWidth);
			endArrow.graphics.lineTo(lineHeight /2, _size * scaleFactor);
			endArrow.graphics.lineTo(lineHeight - lineArrowHeadPadding, _size * scaleFactor - lineArrowWidth);
			
			endLine.graphics.clear();
			endLine.graphics.lineStyle(1, 0x111111, 1);
			endLine.graphics.moveTo(0, _size * scaleFactor);
			endLine.graphics.lineTo(lineHeight, _size * scaleFactor);
			
			label.text = (_size * 10).toString();
			label.rotationZ =-90;
			label.width = 60;
			label.height = lineHeight;
			label.x = 0;
			label.y = _size * scaleFactor/2 +  label.height/2;
			
		}
	}
}