package objects
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import core.AppManager;
	
	import data.Settings;
	
	public class DrawingArea extends Sprite
	{
		private var backgroundShape: Shape;
		private var panalsShapesArray:Array;
		private var horScaleMark:MeasurementScaleHorizontalMark;
		private var verScaleMark:MeasurementScaleVerticalMark;
		private var horizontalAxisLabel:TextField;
		private var verticalAxisLabel:TextField;
		private var labelTextFormat:TextFormat;
		private var rect:Shape;
		private var currentShapeWidth:Number;
		private var currentShapeHeight:Number;
		private var scaleFactor:Number;
		private var standardPanelHorMarker:PanelDimentionHorizontalMark;
		private var standardPanelVerMarker:PanelDimentionVerticalMark;
		private var customPanelHorMarker:PanelDimentionHorizontalMark;
		private var customPanelVerMarker:PanelDimentionVerticalMark;
		
		public function DrawingArea()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			panalsShapesArray = new Array;
			
			backgroundShape = new Shape;
			this.addChild(backgroundShape);
			backgroundShape.graphics.lineStyle(2, 0x990000, 1);
			backgroundShape.graphics.beginFill(0xffffff,1);
			backgroundShape.graphics.drawRect(0, 0, Settings.drawingAreaWidth, Settings.drawingAreaHeight);
			backgroundShape.graphics.endFill(); 
			trace(this,"Settings.drawingAreaWidth",Settings.drawingAreaWidth,"Settings.drawingAreaHeight",Settings.drawingAreaHeight);
			trace(this,"backgroundShape.width",backgroundShape.width,"backgroundShape.height",backgroundShape.height)
			trace(this,"width",this.width,"height",this.height,"scaleX",this.scaleX,"scaleY",this.scaleY);
			
		}
		
		public function updateSize():void
		{
			trace(this,"updateSize()");
//			if(addedShapesArray.length > 0)
//			{
//				for(var i:int=0; i<addedShapesArray.length; i++)
//				{
//					var shape:Shape = addedShapesArray[0];
//					shape.scaleX = shape.scaleY = Math.min(this.scaleX,this.scaleY);
//				}
//			}
			backgroundShape.width = Settings.drawingAreaWidth;
			backgroundShape.height = Settings.drawingAreaHeight;
			trace(this,"width",this.width,"height",this.height,"scaleX",this.scaleX,"scaleY",this.scaleY);
			if(rect != null)
			{
				drawRect(currentShapeWidth,currentShapeHeight);
			}
//			trace(this,"backgroundShape.width",backgroundShape.width,"backgroundShape.height",backgroundShape.height)
			
		}
		
		public function drawRect(shapeWidth:Number, shapeHeight:Number):void
		{
			
			currentShapeWidth = shapeWidth;
			currentShapeHeight = shapeHeight;
			scaleFactor = Settings.drawingScaleFactor;
			
			var positionX:Number;
			var positionY:Number;
			trace(this, "drawRect()");
			if(!rect)
			{
				labelTextFormat = new TextFormat("Calibri",20);
				labelTextFormat.align = TextFormatAlign.CENTER;
				
				horizontalAxisLabel = new TextField;
				horizontalAxisLabel.defaultTextFormat = labelTextFormat;
				this.addChild(horizontalAxisLabel);
				
				verticalAxisLabel = new TextField;
				verticalAxisLabel.defaultTextFormat = labelTextFormat;
				this.addChild(verticalAxisLabel);
				
				horScaleMark = new MeasurementScaleHorizontalMark;
				this.addChild(horScaleMark);
				verScaleMark = new MeasurementScaleVerticalMark;
				this.addChild(verScaleMark);
				
				standardPanelHorMarker = new PanelDimentionHorizontalMark;
				this.addChild(standardPanelHorMarker);
				standardPanelVerMarker = new PanelDimentionVerticalMark;
				this.addChild(standardPanelVerMarker);
				
				customPanelHorMarker = new PanelDimentionHorizontalMark;
				customPanelVerMarker = new PanelDimentionVerticalMark;
				
				rect = new Shape;
				this.addChild(rect);
				
			}
			
			positionX = Settings.drawingAreaWidth*0.5 - currentShapeWidth  * scaleFactor *0.5;
			positionY = Settings.drawingAreaHeight*0.5 - currentShapeHeight  * scaleFactor *0.5;
			
			horizontalAxisLabel.width = 150;
			horizontalAxisLabel.height = 28;
			horizontalAxisLabel.x = Settings.drawingAreaWidth/2 - horizontalAxisLabel.width /2;
			horizontalAxisLabel.y = 2;
//			horizontalAxisLabel.background = true;
//			horizontalAxisLabel.backgroundColor = 0x990000;
			
			verticalAxisLabel.rotationZ = -90;
			verticalAxisLabel.width = 150;
			verticalAxisLabel.height = 28;
			verticalAxisLabel.x = 2;
			verticalAxisLabel.y = Settings.drawingAreaHeight/2 + verticalAxisLabel.height /2;
			
//			if(currentShapeWidth > currentShapeHeight)
//			{
				horizontalAxisLabel.text = "X Axis";
				verticalAxisLabel.text = "Y Axis";
//			}else{
//				horizontalAxisLabel.text = "Y Axis";
//				verticalAxisLabel.text = "X Axis";
//			}
			
			horScaleMark.draw(shapeWidth);
			horScaleMark.x = positionX;
			horScaleMark.y = 30;
			
			verScaleMark.draw(shapeHeight);
			verScaleMark.x = 30;
			verScaleMark.y = positionY;
			
			rect.graphics.clear();
//			rect.scaleX = rect.scaleY = Math.min(rect.scaleY , this.scaleX);
			
			
			
			//draw panals start here
			if (panalsShapesArray.length >0)
			{
				for(var k:int=0; k<panalsShapesArray.length; k++)
				{
					var shape:Shape = panalsShapesArray[k] as Shape;
					shape.graphics.clear();
					this.removeChild(shape);
					shape = null;
				}
				panalsShapesArray = [];
			}
			if(this.contains(customPanelHorMarker))
			{
				this.removeChild(customPanelHorMarker);
			}
			if(this.contains(customPanelVerMarker))
			{
				this.removeChild(customPanelVerMarker);
			}
				
			var standardPanalsCount_X_Axix:Number;
			var standardPanalsCount_Y_Axix:Number;
			var customPanalsCount_X_Axix:Number;
			var customPanalsCount_Y_Axix:Number;
			var customPanalWidth:Number;
			var customPanalHeight:Number;
			var panalWidth:Number;
			var panalHeight:Number;
			var panalShapeWidth:Number;
			var panalShapeHeight:Number;
			if(Settings.panelOrientation == Settings.PANEL_ORIENTATION_HORIZONTAL)
			{
				panalWidth = Settings.STANDARD_PANAL_WIDTH;
				panalHeight = Settings.STANDARD_PANAL_HEIGHT;
				
			}else{
				panalWidth = Settings.STANDARD_PANAL_HEIGHT;
				panalHeight = Settings.STANDARD_PANAL_WIDTH;			
			}
			
			standardPanelHorMarker.draw(panalWidth);
			standardPanelHorMarker.x = positionX;
			standardPanelHorMarker.y = positionY + shapeHeight * scaleFactor + 5; 
			
			standardPanelVerMarker.draw(panalHeight);
			standardPanelVerMarker.x = positionX + shapeWidth * scaleFactor + 5;
			standardPanelVerMarker.y = positionY ; 
			
			
			customPanalWidth = shapeWidth % panalWidth;
			customPanalHeight = shapeHeight % panalHeight;
			standardPanalsCount_X_Axix = Math.floor(shapeWidth / panalWidth);
			standardPanalsCount_Y_Axix = Math.floor(shapeHeight / panalHeight);
			customPanalsCount_X_Axix = (customPanalWidth > 0) ? 1 : 0;
			customPanalsCount_Y_Axix = (customPanalHeight > 0) ? 1 : 0;
			trace("standardPanalsCount_X_Axix",standardPanalsCount_X_Axix,"standardPanalsCount_Y_Axix",standardPanalsCount_Y_Axix,"customPanalWidth",customPanalWidth,"customPanalHeight",customPanalHeight);
			
			
			AppManager.infoView.updateInfo(standardPanalsCount_X_Axix,standardPanalsCount_Y_Axix,panalWidth,panalHeight,
				customPanalsCount_X_Axix,customPanalsCount_Y_Axix,customPanalWidth,customPanalHeight);
			
			var panalLine:Shape;
			var panalShape:Shape;
			var rectCustomPanal:Shape;
			var standardOutlineColor:uint = 0x4968FF;
			var standardFillColor:uint = 0xBFD9FF;
			var customOutlineColor:uint = 0xAA1C2B;
			var customFillColor:uint = 0xEF9496;
			var outLineColor:uint;
			var fillColor:uint;
			for(var i:int=0; i<standardPanalsCount_Y_Axix + customPanalsCount_Y_Axix; i++)
			{
				if(i>=standardPanalsCount_Y_Axix)
				{
					outLineColor = customOutlineColor;
					fillColor = customFillColor;
					panalShapeHeight = customPanalHeight;
				}else{
					outLineColor = standardOutlineColor;
					fillColor = standardFillColor;
					panalShapeHeight = panalHeight;
				}
				for(var j:int=0; j<standardPanalsCount_X_Axix + customPanalsCount_X_Axix; j++)
				{
					if(j>=standardPanalsCount_X_Axix)
					{
						outLineColor = customOutlineColor;
						fillColor = customFillColor;
						panalShapeWidth = customPanalWidth;
					}else{
						panalShapeWidth = panalWidth;
						if(panalShapeHeight == panalHeight)
						{
							outLineColor = standardOutlineColor;
							fillColor = standardFillColor;
						}
					}
					panalShape = new Shape;
					this.addChild(panalShape);
					panalsShapesArray.push(panalShape);
					panalShape.graphics.lineStyle(1, outLineColor, 1);
					panalShape.graphics.beginFill(fillColor,1);
					var panel_x:Number = positionX + (j*panalWidth  * scaleFactor);
					var panel_y:Number = positionY + (i*panalHeight  * scaleFactor);
					panalShape.graphics.drawRect(panel_x, panel_y, panalShapeWidth * scaleFactor, panalShapeHeight * scaleFactor);
					panalShape.graphics.endFill(); 
				}
			}
			//draw digram border
			rect.graphics.lineStyle(5, 0x222222, 1);
			rect.graphics.beginFill(0xffffff,0);
			rect.graphics.drawRect(positionX, positionY, shapeWidth * scaleFactor, shapeHeight * scaleFactor);
			rect.graphics.endFill(); 
			trace("rect.scaleX",rect.scaleX,"rect.scaleY",rect.scaleY);
			trace(this,"width",this.width,"height",this.height,"scaleX",this.scaleX,"scaleY",this.scaleY);
//			for(var i:int=1; i<standardPanalsCount_X_Axix + customPanalsCount_X_Axix; i++)
//			{
//				panalLine = new Shape;
//				this.addChild(panalLine);
//				panalsLinesArray.push(panalLine);
//				panalLine.graphics.lineStyle(1, 0x257AC4, 1);
//				var _ix:Number = positionX + (i*panalWidth);
//				var _iy:Number = positionY + shapeHeight;
//				panalLine.graphics.moveTo(_ix, positionY);
//				panalLine.graphics.lineTo(_ix, _iy);
//			}
//			for(var j:int=1; j<standardPanalsCount_Y_Axix + customPanalsCount_Y_Axix; j++)
//			{
//				panalLine = new Shape;
//				this.addChild(panalLine);
//				panalsLinesArray.push(panalLine);
//				panalLine.graphics.lineStyle(1, 0x257AC4, 1);
//				var _jx:Number = positionX + shapeWidth;
//				var _jy:Number = positionY + (j*panalHeight);
//				panalLine.graphics.moveTo(positionX, _jy);
//				panalLine.graphics.lineTo(_jx, _jy);
//			}
			
			if(customPanalsCount_X_Axix > 0)
			{
				this.addChild(customPanelHorMarker);
				customPanelHorMarker.draw(customPanalWidth);
				customPanelHorMarker.x = positionX + shapeWidth * scaleFactor - customPanalWidth * scaleFactor;
				customPanelHorMarker.y = positionY + shapeHeight * scaleFactor + 5; 
			}
			if(customPanalsCount_Y_Axix > 0)
			{
				this.addChild(customPanelVerMarker);
				customPanelVerMarker.draw(customPanalHeight);
				customPanelVerMarker.x = positionX + shapeWidth * scaleFactor + 5;
				customPanelVerMarker.y = positionY + shapeHeight * scaleFactor - customPanalHeight * scaleFactor; 
			}
			//draw panals end here
			
//			rect.scaleY = this.scaleX;
		}
	}
}