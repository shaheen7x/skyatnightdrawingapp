package screens
{
	import flash.events.Event;
	
	import core.AppManager;
	
	import data.Settings;
	
	import feathers.controls.Header;
	import feathers.controls.LayoutGroup;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.ILayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import objects.DrawingArea;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.text.TextFormat;

	public class MainView extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerTopLeft:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var layoutHorTopCenter:HorizontalLayout;
		private var layoutHorTopLeft:HorizontalLayout;
		private var mainCont:LayoutGroup;
		private var drawingCont:LayoutGroup;
		private var settingsCont:LayoutGroup;
		
		private var settingsView:SettingsView;
		private var infoView:InfoView;
		private var drawingArea:DrawingArea;
		private var drawingHeader:Header;
		private var drawingPlaceHolder:Quad;
		private var drawingInfoCont:LayoutGroup;
		

		
		public function MainView()
		{
			
		}
		override protected function initialize():void
		{
			//			GlobalData.mainView = this;
			//			trials = GlobalData.remainingTrials;
			
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerTopLeft = new VerticalLayout;
			layoutVerTopLeft.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorTopCenter = new HorizontalLayout;
			layoutHorTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutHorTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorTopLeft = new HorizontalLayout;
			layoutHorTopLeft.verticalAlign =  VerticalAlign.TOP;
			layoutHorTopLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutHorTopLeft;
			
			drawingCont = new LayoutGroup;
			drawingCont.layout = layoutVerTopCenter;
//			drawingCont.backgroundSkin = new Quad(2,2,0xffffff);
			
			drawingHeader = new Header;
			drawingHeader.title = "Skay@Night project";
			drawingHeader.fontStyles = new TextFormat("Verdana", 16, 0x404040,HorizontalAlign.CENTER);
			
			drawingPlaceHolder = new Quad(2,2,0xdddddd);
			
			drawingInfoCont = new LayoutGroup;
			drawingInfoCont.layout = layoutVerMedCenter;
//			drawingInfoCont.backgroundSkin = new Quad(2,2,0x909000);
			
			infoView = new InfoView;
			drawingInfoCont.addChild(infoView);
			
			drawingCont.addChild(drawingHeader);
			drawingCont.addChild(drawingPlaceHolder);
			drawingCont.addChild(drawingInfoCont);
			
			settingsCont = new LayoutGroup;
			settingsCont.layout = layoutVerTopCenter;
//			settingsCont.backgroundSkin = new Quad(2,2,0x990000);
			
			settingsView = new SettingsView;
			settingsCont.addChild(settingsView);
			
			
			mainCont.addChild(drawingCont);
			mainCont.addChild(settingsCont);
			this.addChild(mainCont);	
			
			drawingArea = new DrawingArea;
			Starling.current.nativeOverlay.addChild(drawingArea);
			
			AppManager.mainView = this;
			AppManager.settingsView = settingsView;
			AppManager.drawingArea = drawingArea;
			AppManager.infoView = infoView;
			
//			Starling.current.nativeStage.addEventListener(Event.RESIZE, onStageResized)
		}
		
		protected function onStageResized(event:Event):void
		{
			trace(this,"onStageResized()");
			this.draw();
		}
		override protected function draw():void
		{
			var gap:int = 2;
			layoutVerTopCenter.gap = 2;
//			layoutVerTopCenter.paddingRight = 2;
			layoutHorTopCenter.gap = 10;
			layoutVerMedCenter.gap = 15;
			layoutHorMedCenter.gap = 10;
			
			mainCont.setSize(actualWidth,actualHeight);
			drawingCont.setSize(Settings.drawingAreaWidth,actualHeight);
			settingsCont.setSize(Settings.settingsViewWidth,actualHeight);
			
			drawingHeader.setSize(Settings.drawingAreaWidth-4, 46);
			drawingInfoCont.setSize(Settings.drawingAreaWidth,actualHeight - Settings.drawingAreaHeight - drawingHeader.height - layoutHorTopCenter.gap*0.5);
			infoView.setSize(drawingInfoCont.width,drawingInfoCont.height);
			
			drawingPlaceHolder.width = Settings.drawingAreaWidth;
			drawingPlaceHolder.height = Settings.drawingAreaHeight;
			
			settingsView.setSize(settingsCont.width,settingsCont.height);
			
			trace("drawingArea.scaleX",drawingArea.scaleX,"drawingArea.scaleY",drawingArea.scaleY);
			drawingArea.x = gap;
			drawingArea.y = drawingHeader.height + gap;
			drawingArea.width = drawingPlaceHolder.width;// + gap;
			drawingArea.height = drawingPlaceHolder.height;// + gap;
			trace("drawingArea.scaleX",drawingArea.scaleX,"drawingArea.scaleY",drawingArea.scaleY);
			trace("drawingArea.width",drawingArea.width,"drawingArea.height",drawingArea.height);
//			drawingArea.scaleX = drawingArea.scaleY = 1;
			
		}
	}
}