package screens
{
	import core.AppManager;
	
	import data.Settings;
	
	import feathers.controls.Button;
	import feathers.controls.Check;
	import feathers.controls.Header;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.Radio;
	import feathers.controls.TextInput;
	import feathers.core.FeathersControl;
	import feathers.core.ToggleGroup;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.events.Event;
	import starling.text.TextFormat;
	
	public class SettingsView extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerTopLeft:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var layoutHorMedLeft:HorizontalLayout;
		private var layoutHorTopCenter:HorizontalLayout;
		
		private var mainCont:LayoutGroup;		
		private var bodyCont:LayoutGroup;
		private var header:Header;
		private var axisX_InputCont:LayoutGroup;		
		private var axisX_Label:Label;
		private var axisX_Input:TextInput;
		private var axisX_Unit:Label;
		private var axisY_InputCont:LayoutGroup;
		private var axisY_Label:Label;
		private var axisY_Input:TextInput;
		private var axisY_Unit:Label;
		private var tilesOrientationGroup:ToggleGroup;
		private var tilesVertival:Radio;
		private var tilesHorizontal:Radio;
		private var updateBtn:Button;
		private var checkboxA:Check;
		private var checkboxB:Check;
		private var checkboxC:Check;
		private var footerLabel:Label;
		private var axisInfo_Label:Label;
		private var panelTypeGroup:ToggleGroup;
		private var staticPanel:Radio;
		private var twinklingPanel:Radio;
		private var pricePlanGroup:ToggleGroup;
		private var wholeSalePlan:Radio;
		private var retailPlan:Radio;
		private var pricePlanLabel:Label;
		private var panelTypeLabel:Label;
		
		public function SettingsView()
		{
//			super();
		}
		override protected function initialize():void
		{			
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerTopLeft = new VerticalLayout;
			layoutVerTopLeft.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedLeft = new HorizontalLayout;
			layoutHorMedLeft.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutHorTopCenter = new HorizontalLayout;
			layoutHorTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutHorTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutVerTopCenter;			
//			mainCont.backgroundSkin = new Quad(2,2,0x);
			
			header = new Header;
			header.title = "Diagram View";
			header.fontStyles = new TextFormat("Verdana", 16, 0x404040,HorizontalAlign.CENTER);

			bodyCont = new LayoutGroup;
			bodyCont.layout = layoutVerTopLeft;
			axisInfo_Label = new Label;
			axisInfo_Label.fontStyles = new TextFormat("Verdana", 14, 0xFFD711,HorizontalAlign.LEFT);
			axisInfo_Label.text = "measurment values in mm";
			
			axisX_InputCont = new LayoutGroup;
			axisX_InputCont.layout = layoutHorMedLeft;			
			axisX_Label = new Label;
			axisX_Label.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.CENTER);
			axisX_Label.text = "X axis:";
			axisX_Input = new TextInput;
			axisX_Input.fontStyles = new TextFormat("Verdana", 14, 0x0e0e0e,HorizontalAlign.CENTER);
			axisX_Input.fontStyles.italic = true;
			axisX_Unit = new Label;
			axisX_Unit.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.CENTER);
			axisX_Unit.text = "mm";
			axisX_InputCont.addChild(axisX_Label);
			axisX_InputCont.addChild(axisX_Input);
			axisX_InputCont.addChild(axisX_Unit);
			
			axisY_InputCont = new LayoutGroup;
			axisY_InputCont.layout = layoutHorMedLeft;			
			axisY_Label = new Label;
			axisY_Label.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.CENTER);
			axisY_Label.text = "Y axis:";
			axisY_Input = new TextInput;
			axisY_Input.fontStyles = new TextFormat("Verdana", 14, 0x0e0e0e,HorizontalAlign.CENTER);
			axisY_Input.fontStyles.italic = true;
			axisY_Unit = new Label;
			axisY_Unit.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.CENTER);
			axisY_Unit.text = "mm";
			axisY_InputCont.addChild(axisY_Label);
			axisY_InputCont.addChild(axisY_Input);
			axisY_InputCont.addChild(axisY_Unit);
			
			tilesOrientationGroup = new ToggleGroup();
			tilesHorizontal = new Radio;
			tilesHorizontal.label = "Panels Horizontal";
			tilesHorizontal.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);
			tilesHorizontal.toggleGroup = tilesOrientationGroup;
			tilesVertival = new Radio;
			tilesVertival.label = "Panels Vertical";
			tilesVertival.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);
			tilesVertival.toggleGroup = tilesOrientationGroup;
			tilesOrientationGroup.addEventListener( Event.CHANGE, onTilesOrientationChanges );
			

			
			updateBtn = new Button;
			updateBtn.label = "Update";
			updateBtn.fontStyles = new TextFormat("Verdana", 14, 0x404040,HorizontalAlign.CENTER);
			updateBtn.addEventListener(starling.events.Event.TRIGGERED,onDrawBtnTriggered);
			
			panelTypeLabel = new Label;
			panelTypeLabel.fontStyles = new TextFormat("Verdana", 14, 0x909090,HorizontalAlign.LEFT);
			panelTypeLabel.fontStyles.bold = true;
			panelTypeLabel.text = "Select Panel Type:";			
			panelTypeGroup = new ToggleGroup();
			staticPanel = new Radio;
			staticPanel.label = "Static Panel";
			staticPanel.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);
			staticPanel.toggleGroup = panelTypeGroup;
			twinklingPanel = new Radio;
			twinklingPanel.label = "Twinkling Panel";
			twinklingPanel.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);
			twinklingPanel.toggleGroup = panelTypeGroup;
			panelTypeGroup.addEventListener( Event.CHANGE, onPanelTypeChanges );
			
			pricePlanLabel = new Label;
			pricePlanLabel.fontStyles = new TextFormat("Verdana", 14, 0x909090,HorizontalAlign.LEFT);
			pricePlanLabel.fontStyles.bold = true;
			pricePlanLabel.text = "Select Pricing Plan:";
			pricePlanGroup = new ToggleGroup();
			wholeSalePlan = new Radio;
			wholeSalePlan.label = "Wholesale Pricing";
			wholeSalePlan.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);
			wholeSalePlan.toggleGroup = pricePlanGroup;
			retailPlan = new Radio;
			retailPlan.label = "Retail Pricing";
			retailPlan.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);
			retailPlan.toggleGroup = pricePlanGroup;
			pricePlanGroup.addEventListener( Event.CHANGE, onPricePlanChanges );
			
			checkboxA = new Check;
			checkboxA.horizontalAlign = HorizontalAlign.LEFT;
			checkboxA.label = "settings 1";
			checkboxA.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.RIGHT);
			checkboxB = new Check;
			checkboxB.horizontalAlign = HorizontalAlign.LEFT;
			checkboxB.label = "settings 2";
			checkboxB.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.RIGHT);
			checkboxC = new Check;
			checkboxC.horizontalAlign = HorizontalAlign.LEFT;
			checkboxC.label = "settings 3";
			checkboxC.fontStyles = new TextFormat("Verdana", 14, 0xe0e0e0,HorizontalAlign.RIGHT);
			
//			bodyCont.addChild(axisInfo_Label);
			bodyCont.addChild(axisX_InputCont);
			bodyCont.addChild(axisY_InputCont);
			bodyCont.addChild( tilesHorizontal );
			bodyCont.addChild( tilesVertival );
			bodyCont.addChild(updateBtn);
			bodyCont.addChild(panelTypeLabel);
			bodyCont.addChild(staticPanel);
			bodyCont.addChild(twinklingPanel);
			bodyCont.addChild(pricePlanLabel);
			bodyCont.addChild(wholeSalePlan);
			bodyCont.addChild(retailPlan);
//			bodyCont.addChild(checkboxA);
//			bodyCont.addChild(checkboxB);
//			bodyCont.addChild(checkboxC);

			footerLabel = new Label;
			footerLabel.wordWrap = true;
			footerLabel.fontStyles = new TextFormat("Verdana", 12, 0xe0e0e0,HorizontalAlign.CENTER);

			
			footerLabel.text = "Sky@Night drawing app"

			mainCont.addChild(header);
			mainCont.addChild(bodyCont);
			mainCont.addChild(footerLabel);
			
			this.addChild(mainCont);
		}
		
		private function onPricePlanChanges(e:Event):void
		{
			Settings.pricePlan = pricePlanGroup.selectedIndex;
		}
		
		private function onPanelTypeChanges(e:Event):void
		{
			Settings.panalType = panelTypeGroup.selectedIndex;
		}
		
		private function onTilesOrientationChanges(e:Event):void
		{
			Settings.panelOrientation = tilesOrientationGroup.selectedIndex;
		}
		
		private function onDrawBtnTriggered(e:starling.events.Event):void
		{
//			if(axisX_Input.text)
			var w:Number = Number(axisX_Input.text)/10;
			var h:Number = Number(axisY_Input.text)/10;
			if(w > Settings.DRAWING_MAX_WIDTH)
			{
//				w = Settings.DRAWING_MAX_WIDTH;
				Settings.drawingScaleFactor = Settings.DRAWING_MAX_WIDTH / w;
			}else{
				Settings.drawingScaleFactor = 1;
			}
			if(w < 50)
				w = 50;
//			axisX_Input.text = w.toString();
			if((h *Settings.drawingScaleFactor)> Settings.DRAWING_MAX_HEIGHT)
			{
//				h = Settings.DRAWING_MAX_HEIGHT;
				Settings.drawingScaleFactor = Settings.DRAWING_MAX_HEIGHT / h;
//				Settings.drawingScaleFactor = Settings.DRAWING_MAX_HEIGHT / (h * Settings.drawingScaleFactor);
			}
			if(h < 50)
				h = 50;
//			axisY_Input.text = h.toString();
			AppManager.drawingArea.drawRect(w,h);
		}		
		

		override protected function draw():void
		{
			layoutVerTopCenter.gap = 15;
			layoutVerMedCenter.gap = 15;
			layoutHorMedCenter.gap = 10;
			layoutVerTopLeft.padding = 30;
//			layoutVerTopLeft.paddingLeft = 30;
			layoutVerTopLeft.gap = 15;
			
//			trace ("actualWidth",actualWidth,"actualHeight",actualHeight);
			mainCont.setSize(actualWidth,actualHeight);
			
			header.setSize(actualWidth, 46);
//			infoLabel.setSize(actualWidth, 15);
//			trialsLabel.setSize(actualWidth, 15);
			footerLabel.setSize(actualWidth, 15);
			
			bodyCont.setSize(actualWidth,actualHeight - header.height - footerLabel.height - 35);
			
			axisInfo_Label.setSize(190 ,20);
			axisX_InputCont.setSize(actualWidth - 15*2,35);
			axisX_Label.setSize(60 ,20);
			axisX_Input.setSize(100 ,35);
			axisX_Unit.setSize(40 ,20);
			
			axisY_InputCont.setSize(actualWidth - 15*2,35);
			axisY_Label.setSize(60 ,20);
			axisY_Input.setSize(100 ,35);
			axisY_Unit.setSize(40 ,20);
			
			updateBtn.setSize(60+80 ,40);
			
			panelTypeLabel.setSize(190 ,20);
			pricePlanLabel.setSize(190 ,20);
			
			checkboxA.setSize(140,20);
			checkboxB.setSize(140,20);
			checkboxC.setSize(140,20);
			

		}
		

	}
}