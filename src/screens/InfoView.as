package screens
{
	import data.Settings;
	
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.TextArea;
	import feathers.core.FeathersControl;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	
	import starling.text.TextFormat;
	
	public class InfoView extends FeathersControl
	{
		private var layoutVerTopCenter:VerticalLayout;
		private var layoutVerTopLeft:VerticalLayout;
		private var layoutVerMedCenter:VerticalLayout;
		private var layoutHorMedCenter:HorizontalLayout;
		private var layoutHorMedLeft:HorizontalLayout;
		private var layoutHorTopCenter:HorizontalLayout;
		
		private var mainCont:LayoutGroup;
		
		private var panelsInfoLabel:Label;
		private var priceInfoLabel:Label;
		
		public function InfoView()
		{
//			super();
		}
		override protected function initialize():void
		{			
			layoutVerTopCenter = new VerticalLayout;
			layoutVerTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutVerTopLeft = new VerticalLayout;
			layoutVerTopLeft.verticalAlign =  VerticalAlign.TOP;
			layoutVerTopLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutVerMedCenter = new VerticalLayout;
			layoutVerMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutVerMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			
			layoutHorMedCenter = new HorizontalLayout;
			layoutHorMedCenter.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			layoutHorMedLeft = new HorizontalLayout;
			layoutHorMedLeft.verticalAlign =  VerticalAlign.MIDDLE;
			layoutHorMedLeft.horizontalAlign = HorizontalAlign.LEFT;
			
			layoutHorTopCenter = new HorizontalLayout;
			layoutHorTopCenter.verticalAlign =  VerticalAlign.TOP;
			layoutHorTopCenter.horizontalAlign = HorizontalAlign.CENTER;
			
			mainCont = new LayoutGroup;
			mainCont.layout = layoutHorTopCenter;
			
			panelsInfoLabel = new Label;
			panelsInfoLabel.fontStyles = new TextFormat("Arial", 14, 0xe0e0e0,HorizontalAlign.LEFT,VerticalAlign.TOP);
			panelsInfoLabel.wordWrap = true;
			
			priceInfoLabel = new Label;
			priceInfoLabel.fontStyles = new TextFormat("Arial", 14, 0xe0e0e0,HorizontalAlign.LEFT,VerticalAlign.TOP);
			priceInfoLabel.wordWrap = true;
			
			mainCont.addChild(panelsInfoLabel);
			mainCont.addChild(priceInfoLabel);
			
			this.addChild(mainCont);
		}
		
		public function updateInfo(spCountX:int,spCountY:int,spWidth:Number,spHeight:Number,cpCountX:int,cpCountY:int,cpWidth:Number,cpHeight:Number):void
		{
			var totalSpCount:int = spCountY * spCountX;
			var infoText:String = "";
			infoText = "count of SP size \""+ spWidth*10 + " x " + spHeight*10 +"\" "+ totalSpCount +" panal";
			trace("infoText",infoText);
			trace("count of SP =",spCountX,"*",spCountY,"=",spCountY * spCountX);
			if(cpCountX > 0)
			{
				trace("count of CP size",cpWidth*10,"x",spHeight*10,spCountY,"panal");
				infoText +=  "\ncount of CP size \""+ cpWidth * 10 +" x "+ spHeight * 10 +"\" "+ spCountY +" panal";
			}
			if(cpCountY > 0)
			{
				trace("count of CP size",spWidth*10,"x",cpHeight*10,spCountX,"panal");
				infoText +=  "\ncount of CP size \""+ spWidth * 10 +" x "+ cpHeight * 10 +"\" "+ spCountX +" panal";
			}
			if(cpCountY > 0 && cpCountX > 0)
			{
				trace("count of CP size",cpWidth*10,"x",cpHeight*10,"1 panal");
				infoText +=  "\ncount of CP size \""+ cpWidth * 10 +" x "+ cpHeight * 10 +"\" 1 panal";
			}
			panelsInfoLabel.text = infoText;
			
			var spPrice:Number;
			var totalSpPrice:Number;
			var priceText:String = "";
			if(Settings.panalType == Settings.STATIC_PANEL)// if static
			{
				if(Settings.pricePlan == Settings.WHOLESALE_PLAN)
				{
					spPrice = Settings.STANDARD_WHOLESALE_STATIC_PANEL_PRICE;
				}else{
					spPrice = Settings.STANDARD_RETAIL_STATIC_PANEL_PRICE;
				}
			}else{										// if twinkling
				if(Settings.pricePlan == Settings.WHOLESALE_PLAN)
				{
					spPrice = Settings.STANDARD_WHOLESALE_TWINKLIN_PANEL_PRICE;
				}else{
					spPrice = Settings.STANDARD_RETAIL_TWINKLIN_PANEL_PRICE;
				}
			}
			totalSpPrice = totalSpCount * spPrice * spWidth/100 * spHeight/100;
			priceText = "Standard panels price: $" + totalSpPrice;
			priceInfoLabel.text = priceText;
		}
		
		override protected function draw():void
		{
//			layoutVerTopCenter.gap = 15;
//			layoutVerMedCenter.gap = 15;
//			layoutHorMedCenter.gap = 10;
//			layoutVerTopLeft.padding = 30;
//			layoutVerTopLeft.gap = 15;
			
			mainCont.setSize(actualWidth,actualHeight);
			panelsInfoLabel.setSize(actualWidth * 0.44, actualHeight * 0.98);
			priceInfoLabel.setSize(actualWidth * 0.44, actualHeight * 0.98);
		}
	}
}