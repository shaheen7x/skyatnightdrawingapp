package data
{
	public class Settings
	{
		public static const APPLICATION_START_WIDTH:int = 1000;
		public static const APPLICATION_START_HEIGHT:int = 750;
		private static var _drawingAreaWidth:Number;
		private static var _drawingAreaHeight:Number;
		private static var _settingsViewWidth:Number;
		private static var _settingsViewHeight:Number;
		private static var _infoViewWidth:Number;
		private static var _infoViewHeight:Number;
		private static var _starlingWidth:int;
		private static var _starlingHeight:int;
		private static var _panelOrientation:int = 0;
		private static var _panelType:int = 0;
		private static var _pricePlan:int = 0;
		public static const STANDARD_PANAL_WIDTH:Number = 120;
		public static const STANDARD_PANAL_HEIGHT:Number = 60;
		public static const DRAWING_MAX_WIDTH:Number = 900;
		public static const DRAWING_MAX_HEIGHT:Number = 690;
		
		public static const PANEL_ORIENTATION_HORIZONTAL:int = 0;
		public static const PANEL_ORIENTATION_VERTICAL:int = 1;
		public static const STATIC_PANEL:int = 0;
		public static const TWINKLING_PANEL:int = 1;
		public static const WHOLESALE_PLAN:int = 0;
		public static const RETAIL_PLAN:int = 1;
		public static const STANDARD_WHOLESALE_STATIC_PANEL_PRICE:Number = 127;
		public static const STANDARD_WHOLESALE_TWINKLIN_PANEL_PRICE:Number = 185;
		public static const STANDARD_RETAIL_STATIC_PANEL_PRICE:Number = 175;
		public static const STANDARD_RETAIL_TWINKLIN_PANEL_PRICE:Number = 235;
		
		private static var _customWholeSaleStaticPrice:Number;
		private static var _customWholeSaleTwinklingPrice:Number;
		private static var _customRetailStaticPrice:Number;
		private static var _customRetailTwinklingPrice:Number;
		
		private static var _drawingScaleFactor:Number;
		
		public static function get starlingWidth():int
		{
			return _starlingWidth;
		}

		public static function set starlingWidth(value:int):void
		{
			_starlingWidth = value;
		}

		public static function get starlingHeight():int
		{
			return _starlingHeight;
		}

		public static function set starlingHeight(value:int):void
		{
			_starlingHeight = value;
		}

		public static function get drawingAreaWidth():Number
		{
			return _drawingAreaWidth;
		}

		public static function set drawingAreaWidth(value:Number):void
		{
			_drawingAreaWidth = value;
		}

		public static function get drawingAreaHeight():Number
		{
			return _drawingAreaHeight;
		}

		public static function set drawingAreaHeight(value:Number):void
		{
			_drawingAreaHeight = value;
		}

		public static function get settingsViewWidth():Number
		{
			return _settingsViewWidth;
		}

		public static function set settingsViewWidth(value:Number):void
		{
			_settingsViewWidth = value;
		}

		public static function get settingsViewHeight():Number
		{
			return _settingsViewHeight;
		}

		public static function set settingsViewHeight(value:Number):void
		{
			_settingsViewHeight = value;
		}

		public static function get infoViewWidth():Number
		{
			return _infoViewWidth;
		}

		public static function set infoViewWidth(value:Number):void
		{
			_infoViewWidth = value;
		}

		public static function get infoViewHeight():Number
		{
			return _infoViewHeight;
		}

		public static function set infoViewHeight(value:Number):void
		{
			_infoViewHeight = value;
		}

		public static function get panelOrientation():int
		{
			return _panelOrientation;
		}

		public static function set panelOrientation(value:int):void
		{
			_panelOrientation = value;
		}

		public static function get drawingScaleFactor():Number
		{
			return _drawingScaleFactor;
		}

		public static function set drawingScaleFactor(value:Number):void
		{
			_drawingScaleFactor = value;
		}

		public static function get panalType():int
		{
			return _panelType;
		}

		public static function set panalType(value:int):void
		{
			_panelType = value;
		}

		public static function get pricePlan():int
		{
			return _pricePlan;
		}

		public static function set pricePlan(value:int):void
		{
			_pricePlan = value;
		}

		public static function get customWholeSaleStaticPrice():Number
		{
			return _customWholeSaleStaticPrice;
		}

		public static function set customWholeSaleStaticPrice(value:Number):void
		{
			_customWholeSaleStaticPrice = value;
		}

		public static function get customWholeSaleTwinklingPrice():Number
		{
			return _customWholeSaleTwinklingPrice;
		}

		public static function set customWholeSaleTwinklingPrice(value:Number):void
		{
			_customWholeSaleTwinklingPrice = value;
		}

		public static function get customRetailStaticPrice():Number
		{
			return _customRetailStaticPrice;
		}

		public static function set customRetailStaticPrice(value:Number):void
		{
			_customRetailStaticPrice = value;
		}

		public static function get customRetailTwinklingPrice():Number
		{
			return _customRetailTwinklingPrice;
		}

		public static function set customRetailTwinklingPrice(value:Number):void
		{
			_customRetailTwinklingPrice = value;
		}


	}
}