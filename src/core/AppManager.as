package core
{
	import objects.DrawingArea;
	
	import screens.InfoView;
	import screens.MainView;
	import screens.SettingsView;

	public class AppManager
	{
		private static var _mainView:MainView;
		private static var _settingsView:SettingsView;
		private static var _drawingArea:DrawingArea;
		private static var _infoView:InfoView;
		
		public function AppManager()
		{
		}

		public static function get mainView():MainView
		{
			return _mainView;
		}

		public static function set mainView(value:MainView):void
		{
			_mainView = value;
		}

		public static function get drawingArea():DrawingArea
		{
			return _drawingArea;
		}

		public static function set drawingArea(value:DrawingArea):void
		{
			_drawingArea = value;
		}

		public static function get settingsView():SettingsView
		{
			return _settingsView;
		}

		public static function set settingsView(value:SettingsView):void
		{
			_settingsView = value;
		}

		public static function get infoView():InfoView
		{
			return _infoView;
		}

		public static function set infoView(value:InfoView):void
		{
			_infoView = value;
		}


	}
}