package core
{
	import flash.events.Event;
	
	import feathers.FEATHERS_VERSION;
	
	import screens.MainView;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import themes.CustomTheme;
	
	public class MainStarling extends Sprite
	{
		public static var theme:CustomTheme;
		private var mainView:MainView;
		
		public function MainStarling()
		{
			super();
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			
		}
		private function onAddedToStage(e:starling.events.Event):void
		{
			this.removeEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			trace("MainApp added to Stage");
//			Assets.init(this);
			StartApp()
		}
		
		
		public function StartApp():void
		{
			theme = new CustomTheme;
			trace("STARLING VERSION "+Starling.VERSION);
			trace("FEATHERS VERSION "+FEATHERS_VERSION);
			
			mainView = new MainView;
			mainView.setSize(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight);
			this.addChild(mainView);
			
			Starling.current.nativeStage.addEventListener(flash.events.Event.RESIZE, onStageResized);
		}
		public function onStageResized(e:flash.events.Event):void
		{
//			trace(this,"onStageResized()");
			mainView.setSize(Starling.current.stage.stageWidth,Starling.current.stage.stageHeight);
			AppManager.drawingArea.updateSize();
//			mainView.invalidate();
//			trace("Starling.current.stage.stageWidth",Starling.current.stage.stageWidth,"Starling.current.stage.stageHeight",Starling.current.stage.stageHeight);
//			trace("mainView.width",mainView.width,"mainView.height",mainView.height);
		}
	}
}